CONTENTS OF THIS FILE
---------------------
* Introduction
* Features
* Requirements
* Installation
* Known problems
* Future plans
* Maintainers
* Credits

INTRODUCTION
------------

Drupal core's IP address blocking functionality (at
admin/config/people/ip-blocking) is a great way to ban IP addresses that fill
your logs with 403s to node/add or failed login attempts.

But, blocked IP addresses without expiry dates will never be unblocked, meaning
potentially-legitimate visitors who previously had a virus or took over an IP
address that used to belong to a spammer will never be able to access your site
ever again.

Blocked IPs Expire aims to be a simple, lightweight, stable, tested, expandable
solution for adding expiry dates to blocked IP addresses and unblocking them
after a certain amount of time.

FEATURES
--------

* IP addresses are unblocked on the next cron run after their expiry date has
  passed,
* Modifies the existing IP address blocking administration form to accept an
  expiry date,
* Modifies the existing IP address blocking administration list to show the
  expiry date for each blocked IP address,
* Provides a globally-configurable default time to expiry, and,
* Provides a hook to allow other modules to act when an IP address is about to
  be deleted.

REQUIREMENTS
------------

    Drupal 7

INSTALLATION
------------

1. Download and install the blocked_ips_expire project.

   See https://www.drupal.org/node/895232 for further information.

2. Add or edit a rule. Add an action. From the "Blocked IPs" group, choose
   "Block IP address with expiry date". Enter the IP address to block (you can
   use Tokens), enter an expiry date, and click the "Save" button.

KNOWN PROBLEMS
--------------

We don't know of any problems at this time, so if you find one, please let us
know by adding an issue!

FUTURE PLANS
------------

I don't have any specific plans beyond the current functionality, but I'm open
to patches so long as they don't over-complicate the module.

MAINTAINERS
-----------

* M Parker (mparker17)
* Renaud Joubert (bisonbleu)

CREDITS
-------

MallDatabase.com helped mparker17 to come up with the concept and work on the
module.

OpenConcept Consulting Inc. paid mparker17 to work on the module.
