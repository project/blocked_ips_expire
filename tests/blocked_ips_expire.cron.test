<?php

/**
 * @file
 * Contains \BlockedIpsExpireCron.
 */

/**
 * Tests that expired IPs will be unblocked on cron run.
 */
class BlockedIpsExpireCron extends BlockedIpsExpireTestCase {

  /**
   * An IP address that has expired.
   *
   * @var string
   */
  protected $expiredIpAddress;

  /**
   * The expiry date for an IP address that has expired.
   *
   * @var int
   */
  protected $expiredDate;

  /**
   * An IP address that will expire in the future.
   *
   * @var string
   */
  protected $notYetExpiredIpAddress;

  /**
   * The expiry date for an IP address that will expire in the future.
   *
   * @var int
   */
  protected $notYetExpiredDate;

  /**
   * A user with administrative permissions.
   *
   * @var object
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Blocked IPs Expire: Cron tests',
      'description' => 'Tests that expired IPs will be unblocked on cron run.',
      'group' => 'Blocked IPs Expire',
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    // SimpleTest Turbo makes things really fast. Let's use it if we can.
    if (module_exists('simpletest_turbo')) {
      $id = simpletest_turbo_id('simpletest_turboOptimized');
      if (db_table_exists('fasttest' . $id . 'node')) {
        $this->prepareDatabasePrefix();
        $this->prepareEnvironment();
        $this->changeDatabasePrefix();
        simpletest_turbo_copy_tables('fasttest' . $id, $this->databasePrefix);
        variable_set('file_public_path', $this->public_files_directory);
        variable_set('file_private_path', $this->private_files_directory);
        variable_set('file_temporary_path', $this->temp_files_directory);
        $this->setup = TRUE;
      }
      else {
        parent::setUp();
        simpletest_turbo_copy_tables($this->databasePrefix, 'fasttest' . $id);
      }
    }
    else {
      parent::setUp();
    }

    // Set up variables.
    $this->expiredIpAddress = BlockedIpsExpireCron::generateIpAddress();
    $this->expiredDate = BlockedIpsExpireCron::randomDatetimeInPast();
    $this->notYetExpiredIpAddress = BlockedIpsExpireCron::generateIpAddress();
    $this->notYetExpiredDate = BlockedIpsExpireCron::randomDatetimeInFuture();

    // Log in as an administrative user.
    $this->adminUser = $this->drupalCreateUser(array(
      'access content',
      'access administration pages',
      'administer site configuration',
    ));
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test that running cron removes expired IP addresses.
   */
  public function testCronRemovesExpiredIpAddresses() {
    // Create an IP address that expired in the past and an IP address that will
    // expire in the future.
    _blocked_ips_expire_add_ip($this->expiredIpAddress, $this->expiredDate);
    _blocked_ips_expire_add_ip($this->notYetExpiredIpAddress, $this->notYetExpiredDate);

    // Check that the IP addresses exist beforehand.
    $in_db = BlockedIpsExpireCron::isIpInDatabase($this->expiredIpAddress);
    $this->assertTrue($in_db,
      'Expired IP in database before cron run.'
    );
    $in_db = BlockedIpsExpireCron::isIpInDatabase($this->notYetExpiredIpAddress);
    $this->assertTrue($in_db,
      'Not-yet-expired IP in database before cron run.'
    );

    // Run cron.
    $this->cronRun();

    // Test that the IP addresses that expired before cron was run have been
    // deleted.
    $in_db = BlockedIpsExpireCron::isIpInDatabase($this->expiredIpAddress);
    $this->assertFalse($in_db,
      'Expired IP not in database after cron run.'
    );

    // Test that the IP addresses that expire after cron was run are still
    // there.
    $in_db = BlockedIpsExpireCron::isIpInDatabase($this->notYetExpiredIpAddress);
    $this->assertTrue($in_db,
      'Not-yet-expired IP in database after cron run.'
    );
  }

}
