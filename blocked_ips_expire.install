<?php

/**
 * @file
 * Install, update, and uninstall functions for the blocked_ips_expire module.
 */

/**
 * Implements hook_requirements().
 */
function blocked_ips_expire_requirements($phase) {
  $requirements = array();
  $t = get_t();

  // Report IP addresses with unassigned expiry dates at runtime only.
  if ($phase === 'runtime') {
    $ips_without_expiry_dates = _blocked_ips_expire_get_unassigned(TRUE);
    if ($ips_without_expiry_dates > 0) {
      $requirements['blocked_ips_expire'] = array(
        'title' => $t('Blocked IP expiry'),
        'severity' => REQUIREMENT_WARNING,
        'value' => format_plural($ips_without_expiry_dates,
          'An IP address has not been given an expiry date.',
          '@count IP addresses have not been given expiry dates.'
        ),
        'description' => $t("Blocked IP addresses without expiry dates will never be unblocked, meaning potentially-legitimate visitors who previously had a virus or took over an IP address that used to belong to a spammer will never be able to access your site. Visit <a href='@bulk_assign_url'>the bulk assignment page</a> to assign expiry dates to blocked IP addresses that don't have them.", array(
          '@bulk_assign_url' => base_path() . 'admin/config/people/blocked_ips_expire/bulk_assign',
        )),
      );
    }
  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function blocked_ips_expire_schema() {
  $schema = array();

  // A table that can be joined onto blocked_ips to add an expiry date to that
  // entry.
  $schema['blocked_ips_expire'] = array(
    'description' => '',
    'fields' => array(
      'iid' => array(
        'description' => 'Primary Key: unique ID for IP addresses.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'expiry_date' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Timestamp for when this IP address can be unblocked.',
      ),
    ),
    'primary key' => array('iid'),
    'indexes' => array(
      'iid' => array('iid'),
    ),
    'foreign keys' => array(
      'iid' => array(
        'table' => 'blocked_ips',
        'columns' => array('iid' => 'iid'),
      ),
    ),
  );

  return $schema;
}

/**
 * Installs blocked_ips_expire module.
 */
function blocked_ips_expire_update_7000(&$sandbox) {
  // Noop to ensure Drupal stores a schema version in the database.
}
