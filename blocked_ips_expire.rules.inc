<?php

/**
 * @file
 * Rules hooks for the blocked_ips_expire module.
 */

/**
 * Implements hook_rules_action_info().
 */
function blocked_ips_expire_rules_action_info() {
  $actions = array();

  // Add an action to block an IP address.
  $actions['blocked_ips_expire_block_ip'] = array(
    'group' => t('Blocked IPs Expire'),
    'label' => t('Block IP address with expiry date'),
    'parameter' => array(
      'ip' => array(
        'type' => 'text',
        'label' => t('IP address to block'),
        'description' => t('If not provided, the IP address of the current user will be used.'),
        'optional' => TRUE,
        'default value' => NULL,
      ),
      'expiry_date' => array(
        'type' => 'date',
        'label' => t('Expiry date'),
      ),
    ),
    'base' => '_blocked_ips_expire_add_ip',
  );

  return $actions;
}
