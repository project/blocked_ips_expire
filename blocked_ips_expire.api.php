<?php

/**
 * @file
 * Hooks provided by the blocked_ips_expire module.
 */

/**
 * Act on a blocked IP address being deleted when it expires.
 *
 * This hook is only run when a blocked IP address is deleted because it has
 * expired, to allow other modules to perform some other action.
 *
 * @param object $ip_info
 *   An object with the following properties containing information about the IP
 *   address:
 *   - 'iid': The IID of the IP address. Will be NULL if this IP address is new.
 *   - 'ip': The IP address to block.
 *   - 'expiry_date': The expiry date of the IP address. Will be NULL if this IP
 *     address is new.
 *
 * @ingroup hooks
 */
function hook_blocked_ips_expire_deleting($ip_info) {
  // Show a message when the IP address gets deleted.
  if (user_access('block IP addresses')) {
    drupal_set_message(t("The blocked IP address @ip reached it's expiry date and was unblocked.", array(
      '@ip' => $ip_info->ip,
    )));
  }
}
