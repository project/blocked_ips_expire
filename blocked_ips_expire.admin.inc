<?php

/**
 * @file
 * An administrative preference page for the blocked_ips_expire module.
 */

/**
 * Page callback: Form constructor for the blocked IPs expire preference page.
 *
 * @see blocked_ips_expire_menu()
 * @see system_settings_form()
 *
 * @ingroup forms
 */
function _blocked_ips_expire_preferences($form = array(), &$form_state = array()) {
  // The default length of time an IP address should be blocked for.
  $form['blocked_ips_expire_default_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Default expiry time for blocked IP addresses'),
    '#element_validate' => array('_blocked_ips_expire_element_validate_strtotime'),
    '#default_value' => variable_get('blocked_ips_expire_default_time', '+2 years'),
    '#description' => t("Must be in a format that <a href='@strtotime'>PHP's strtotime function</a> can interpret. Setting to <code>+2 years</code> or more is recommended. Other modules will get the opportunity to modify this.", array(
      '@strtotime' => 'https://php.net/manual/function.strtotime.php',
    )),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
