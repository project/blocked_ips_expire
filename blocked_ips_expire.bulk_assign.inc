<?php

/**
 * @file
 * An administrative page to assign expiry dates to IP addresses in bulk.
 */

/**
 * Page callback: Form constructor for a bulk-assign-expiry-times form.
 *
 * Lists the IP addresses that haven't been assigned expiry dates and prompts
 * the user for a relative or fixed date to set them to. Assigns them in bulk on
 * submission.
 *
 * @see blocked_ips_expire_menu()
 *
 * @ingroup forms
 */
function _blocked_ips_expire_bulk_assign($form = array(), $form_state = array()) {
  $form = array();
  $iids_to_process = array();

  // Get a list of IP addresses that haven't been assigned expiry dates as well
  // as a count of the number there are.
  $query = db_select('blocked_ips', 'bi');
  $query->leftJoin('blocked_ips_expire', 'bie', 'bi.iid = bie.iid');
  $query->addField('bi', 'iid');
  $query->addField('bi', 'ip');
  $query->isNull('expiry_date');
  $result = $query->execute();
  $count = $query->countQuery()->execute()->fetchField();

  // Display the whole form if there are IP addresses that need expiry dates...
  if ($count > 0) {
    $headers = array();
    $rows = array();

    // Parse the results of the query.
    // We're going to display each IP address itself in the table and store the
    // corresponding iid in a form value. This kinda mixes view-related things
    // with controller-related things for the sake of efficiency.
    // Start by setting table headers for display.
    $headers[] = t('IP address');
    foreach ($result as $value) {
      // Add a row to the table (view).
      $rows[] = array($value->ip);

      // Add a row to the data (controller).
      $iids_to_process[] = $value->iid;
    }

    // List the unassigned IP addresses.
    $form['ips_without_expiry_date'] = array(
      '#prefix' => '<p>' . t('The following IP addresses have not been assigned expiry dates:') . '</p>',
      '#markup' => theme('table', array(
        'header' => $headers,
        'rows' => $rows,
      )),
      '#suffix' => '<p>' . t('Blocked IP addresses without expiry dates will never be unblocked, meaning potentially-legitimate visitors who previously had a virus or took over an IP address that used to belong to a spammer will never be able to access your site.') . '</p>',
    );

    // Explain what this form does.
    $form['bulk_assign_form_prompt'] = array(
      '#markup' => '<p>' . t('This form will allow you to assign a new expiry date to all IP addresses that do not have one in bulk.') . '</p>',
    );

    // Prompt for a expiry date to assign.
    $form['bulk_assign_new_date'] = array(
      '#type' => 'textfield',
      '#title' => t('New expiry date'),
      '#maxlength' => 25,
      '#description' => t('Format: %time. The date format is YYYY-MM-DD and %timezone is the time zone offset from UTC. Leave blank to use the time of form submission.', array(
        '%time' => date_format(date_create(), 'Y-m-d H:i:s O'),
        '%timezone' => date_format(date_create(), 'O'),
      )),
      '#element_validate' => array('_blocked_ips_expire_element_validate_strtotime_or_empty'),
    );

    // The buttons.
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Assign new expiry date to all IP addresses without one already'),
    );
  }
  // ... otherwise display a message saying there are none to assign.
  else {
    // Explain what this form does.
    $form['bulk_assign_form_prompt'] = array(
      '#markup' => '<p>' . t('This form will allow you to assign a new expiry date to all IP addresses that do not have one in bulk.') . '</p>',
    );

    // Say there's nothing to do.
    $form['bulk_assign_nothing_to_do'] = array(
      '#markup' => '<p>' . t('No IP addresses need expiry dates. <a href="!list">See the list of blocked IP addresses.</a>', array(
        '!list' => base_path() . 'admin/config/people/ip-blocking',
      )) . '</p>',
    );
  }

  // Either way, store the addresses that need expiry dates, even if it's just
  // an empty array.
  $form['iids_without_expiry_dates'] = array(
    '#type' => 'value',
    '#value' => $iids_to_process,
  );

  return $form;
}

/**
 * Form submission handler for _blocked_ips_expire_bulk_assign().
 *
 * @see _blocked_ips_expire_bulk_assign()
 */
function _blocked_ips_expire_bulk_assign_submit($form, &$form_state) {
  $new_date = $form_state['values']['bulk_assign_new_date'];
  $bulk_assign_batch = array();

  // Convert the user's date submission into a UNIX timestamp.
  if (empty($new_date)) {
    $new_date = strtotime('now');
  }
  else {
    $new_date = strtotime($new_date);
  }

  // Create a batch.
  $bulk_assign_batch['title'] = t('Assigning expiry date');
  foreach ($form_state['values']['iids_without_expiry_dates'] as $iid) {
    $bulk_assign_batch['operations'][] = array(
      '_blocked_ips_expire_bulk_assign_batch_operation',
      array($new_date, $iid),
    );
  }
  $bulk_assign_batch['finished'] = '_blocked_ips_expire_bulk_assign_batch_finished';

  // Add the batch to the list of batches to process.
  batch_set($bulk_assign_batch);
}
